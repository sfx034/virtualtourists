//
//  FlickrHelper.swift
//  VirtualTourists
//
//  Created by Volodymyr Mykhailiuk on 24.11.2019.
//  Copyright © 2019 Volodymyr Mykhailiuk. All rights reserved.
//

import Alamofire

class FlickrHelper {
    
    static let shared = FlickrHelper()
    
    lazy var consumerKey: String = {
        let dict = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "Info", ofType: "plist")!) as! Dictionary<String, AnyObject>
        return dict["FlickrConsumerKey"] as! String
    }()
    
    func fetchFlickrPhotos(by latitude: String, and longitude: String, completion: (([String]?) -> Void)? = nil) {
        let url = URL(string: "https://api.flickr.com/services/rest/")!
        let parameters :Dictionary = [
                   "method"         : "flickr.photos.search",
                   "lat"            : latitude,
                   "lon"            : longitude,
                   "api_key"        : consumerKey,
                   "radius"         : "0.1",
                   "format"         : "json",
                   "nojsoncallback" : "1",
                   "extras"         : "url_z"
               ]
        
        Alamofire.request(url, method: .get, parameters: parameters)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success:
                    guard let data = response.data else {
                        return
                    }
                    
                    do {
                        let searchPhotosJSON = try JSONDecoder().decode(SearchPhotosJSON.self, from: data)
                        let urls = searchPhotosJSON.photos.photo.map { $0.url_z }.compactMap { $0 }
                        completion?(urls)
                    } catch let error {
                        print(error)
                    }
                    
                case .failure:
                    completion?(nil)
                }
        }
    }
}
