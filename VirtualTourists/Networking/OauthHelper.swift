//
//  OauthHelper.swift
//  VirtualTourists
//
//  Created by Volodymyr Mykhailiuk on 23.11.2019.
//  Copyright © 2019 Volodymyr Mykhailiuk. All rights reserved.
//

import OAuthSwift

class OauthHelper {
    static let shared = OauthHelper()
    private var oauthswift: OAuth1Swift?
    private var oauthToken: String?

    lazy var secretKey: String = {
        let dict = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "Info", ofType: "plist")!) as! Dictionary<String, AnyObject>
        return dict["FlieckSecretKey"] as! String
    }()

    lazy var consumerKey: String = {
        let dict = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "Info", ofType: "plist")!) as! Dictionary<String, AnyObject>
        return dict["FlickrConsumerKey"] as! String
    }()


    func authorize() -> OauthHelper {

        oauthswift = OAuth1Swift(
            consumerKey:    consumerKey,
            consumerSecret: secretKey,
            requestTokenUrl: "https://www.flickr.com/services/oauth/request_token",
            authorizeUrl:    "https://www.flickr.com/services/oauth/authorize",
            accessTokenUrl:  "https://www.flickr.com/services/oauth/access_token"
        )

        oauthswift?.authorizeURLHandler = OAuthSwiftOpenURLExternally.sharedInstance
        let _ = oauthswift?.authorize(
        withCallbackURL: URL(string: "oauth-swift://oauth-callback/flickr")!) { result in
            switch result {
            case .success(let (credential, _, _)):
                // todo: implemen token store
                self.testFlickr()
                print("success")
            case .failure(let error):
                print(error.description)
            }
        }
        return self
    }

    func testFlickr() {
        let url :String = "https://api.flickr.com/services/rest/"
        let parameters :Dictionary = [
            "method"         : "flickr.photos.geo.photosForLocation",
            "lat"            : "48.922922",
            "lon"            : "24.706792",
            "accuracy"       : "16",
            "api_key"        : consumerKey,
            "format"         : "json",
            "nojsoncallback" : "1",
        ]

        let _ = oauthswift?.client.get(url, parameters: parameters) { result in
            switch result {
            case .success(let response):
                let jsonDict = try? response.jsonObject() //as? [Dictionary<String, Any>]
//                let data = jsonDict["photo"] as! Data
//                let photo = UIImage(data: data)
                print(jsonDict as Any)
            case .failure(let error):
                print(error)
            }
        }
    }
}
