//
//  PhotoCell.swift
//  VirtualTourists
//
//  Created by Volodymyr Mykhailiuk on 24.11.2019.
//  Copyright © 2019 Volodymyr Mykhailiuk. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.image = nil
    }
}
