//
//  PhotoJSON.swift
//  VirtualTourists
//
//  Created by Volodymyr Mykhailiuk on 27.11.2019.
//  Copyright © 2019 Volodymyr Mykhailiuk. All rights reserved.
//

struct PhotoJSON: Decodable {
    let id: String
    let owner: String?
    let title: String?
    let url_z: String?
}
