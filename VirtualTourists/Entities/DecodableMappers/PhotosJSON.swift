//
//  PhotosJSON.swift
//  VirtualTourists
//
//  Created by Volodymyr Mykhailiuk on 27.11.2019.
//  Copyright © 2019 Volodymyr Mykhailiuk. All rights reserved.
//

struct PhotosJSON: Decodable {
    let pages: Int?
    let total: String?
    let photo: [PhotoJSON]
}
