//
//  PhotoAlbumViewController.swift
//  VirtualTourists
//
//  Created by Volodymyr Mykhailiuk on 20.11.2019.
//  Copyright © 2019 Volodymyr Mykhailiuk. All rights reserved.
//

import UIKit
import OAuthSwift
import CoreLocation


class PhotoAlbumViewController: UIViewController {

    private let itemsPerRow: CGFloat = 3
    private let sectionInsets = UIEdgeInsets(top: 20.0, left: 12.0, bottom: 20.0, right: 12.0)
    private var photosURL: [String] = []
    private var coordinate: CLLocationCoordinate2D? = nil
    
    lazy var cellSize: CGSize = {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
               
        return CGSize(width: widthPerItem, height: widthPerItem)
    }()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPhotos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initNavigationController()
    }

    func set(_ coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
    
    func initNavigationController() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.title = "Photos"
    }

    func loadPhotos() {
        guard let coordinate = coordinate else {
            return
        }

        let latitude = String(coordinate.latitude)
        let longitude = String(coordinate.longitude)

        FlickrHelper.shared.fetchFlickrPhotos(by: latitude, and: longitude) { photos in
            if let photos = photos {
                self.photosURL = photos
            }

            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
}

extension PhotoAlbumViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosURL.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell

        DispatchQueue.global().async {
            let url = URL(string: self.photosURL[indexPath.row])!
            let data = try? Data(contentsOf: url)
            DispatchQueue.main.async {
                cell.photoImageView.image = UIImage(data: data!)
            }
        }

        cell.layer.cornerRadius = cell.frame.width / 10

        return cell
    }
}

// MARK: - Collection View Flow Layout Delegate
extension PhotoAlbumViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
