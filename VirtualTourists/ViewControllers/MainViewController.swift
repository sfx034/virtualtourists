//
//  ViewController.swift
//  VirtualTourists
//
//  Created by Volodymyr Mykhailiuk on 17.11.2019.
//  Copyright © 2019 Volodymyr Mykhailiuk. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MainViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
        
    override func viewDidLoad() {
        super.viewDidLoad()

        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleTap(gestureReconizer:)))
        gestureRecognizer.delegate = self
        mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
}

//MARK: - UIGestureRecognizerDelegate
extension MainViewController: UIGestureRecognizerDelegate {

    @objc func handleTap(gestureReconizer: UILongPressGestureRecognizer) {
        
        let location = gestureReconizer.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.coordinate = coordinate
        
        mapView.addAnnotation(pointAnnotation)
    }
}

//MARK: - MKMapViewDelegate
extension MainViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "PhotoAlbum", bundle: nil)
        let photosViewController = storyBoard.instantiateViewController(withIdentifier: "photo") as! PhotoAlbumViewController
        photosViewController.set(view.annotation!.coordinate)

        self.navigationController?.pushViewController(photosViewController, animated: true)
        
        //Workaround to allow user select certain pin more than once
        self.mapView.deselectAnnotation(view.annotation, animated: false)
    }
}
